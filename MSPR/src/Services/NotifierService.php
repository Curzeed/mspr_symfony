<?php

namespace App\Services;

use App\Entity\User;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

class NotifierService
{
    private NotifierInterface $notifier;

    public function __construct(NotifierInterface $notifier){
        $this->notifier = $notifier;
    }
    public function createNotification(string $content, string $subject,User $user){
        $notification = new Notification($subject,['email']);
        $notification->content($content);
        $recipient = new Recipient($user->getEmail());
        $this->notifier->send($notification,$recipient);
    }
}