<?php
namespace App\Services;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;

class DiscordService
{
    private string $api_endpoint;
    private string $client_id;
    private string $redirect_uri;
    private string $client_secret;
    private UserPasswordHasherInterface $passHash;

    public function __construct(UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $em)
    {
        $this->api_endpoint = "https://discord.com/api";
        $this->client_id = "1132931331622436934";
        $this->client_secret = "ElBo6AgYaStA7nTmFgaJZnCQm8WixcUI";
        $this->redirect_uri = "https://localhost:8000/login";
        $this->passHash = $passwordHasher;
        $this->em = $em;
    }
    private function getToken($code){
        $data = [
            "client_id" => $this->client_id,
            "client_secret" => $this->client_secret,
            "grant_type" => "authorization_code",
            "code" => $code,
            "redirect_uri" => $this->redirect_uri,
            "scope" => "identify email"
        ];
        $headers = [
            "Content-Type: application/x-www-form-urlencoded"
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST',$this->api_endpoint . "/oauth2/token", [
            'verify' => false, // <-- don't verify SSL certificate
            'headers' => $headers,
            'form_params' => $data
        ]);
        return json_decode($response->getBody()->getContents());
    }
    public function getInfos($code) : User {
        $codes = $this->getToken($code);
        $client = new Client();
        $response = $client->get($this->api_endpoint . "/users/@me", [
            'headers' => [
                'Authorization' => 'Bearer ' . $codes->access_token,
            ],
            'verify' => false, // <-- don't verify SSL certificate
        ]);
        $result = json_decode($response->getBody()->getContents(),true);
        if($this->em->getRepository(User::class)->findOneBy(['email' => $result['email']])){
            return $this->em->getRepository(User::class)->findOneBy(['email' => $result['email']]);
        }else{
            $user = $this->createUser($result);
            return $user;
        }
    }
    private function createUser($dsUser) : User{
        $user = new User();
        $user->setEmail($dsUser['email']);
        $user->setPassword($this->passHash->hashPassword($user,$dsUser['email']));
        $user->setRoles(['ROLE_USER']);
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }
}